import {FETCH_DECODE_SUCCESS, ON_DECODE_MESSAGE_CHANGE} from "../actions/decodeActions";

const initialState = {
    error: null,
    message: '',
    decodedMessage: '',
};

const decodedReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DECODE_SUCCESS: {
            return {...state, decodedMessage: action.payload.decoded};
        }

        case ON_DECODE_MESSAGE_CHANGE: {
            return {...state, message: action.payload};
        }

        default:
            return {...state};
    }
};

export default decodedReducer;