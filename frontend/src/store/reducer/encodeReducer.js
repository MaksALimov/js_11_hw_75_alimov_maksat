import {FETCH_ENCODE_SUCCESS, ON_ENCODE_MESSAGE_CHANGE, ON_PASSWORD_CHANGE} from "../actions/encodeActions";
const initialState = {
    error: null,
    message: '',
    password: '',
    encode: '',
    decode: '',
};

const encodeReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ENCODE_SUCCESS: {
            return {...state, encodedMessage: action.payload.encoded};
        }

        case ON_ENCODE_MESSAGE_CHANGE: {
            return {...state, message: action.payload}
        }

        case ON_PASSWORD_CHANGE: {
            return {...state, password: action.payload};
        }

        default:
            return {...state};
    }
};

export default encodeReducer;