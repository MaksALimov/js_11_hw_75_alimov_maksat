import axios from "axios";

export const FETCH_DECODE_REQUEST = 'FETCH_DECODE_REQUEST';
export const FETCH_DECODE_SUCCESS = 'FETCH_DECODE_SUCCESS';
export const FETCH_DECODE_FAILURE = 'FETCH_DECODE_FAILURE';
export const ON_DECODE_MESSAGE_CHANGE = 'ON_DECODE_MESSAGE_CHANGE';

export const fetchDecodeRequest = () => ({type: FETCH_DECODE_REQUEST});
export const fetchDecodeSuccess = decodedMessage => ({type: FETCH_DECODE_SUCCESS, payload: decodedMessage});
export const fetchDecodeFailure = error => ({type: FETCH_DECODE_FAILURE, payload: error});

export const onDecodeMessageChange = encodeMessage => ({type: ON_DECODE_MESSAGE_CHANGE, payload: encodeMessage});

export const fetchDecode = decodedMessage => {
    return async dispatch => {
      try {
          dispatch(fetchDecodeRequest());

          const response = await axios.post('http://localhost:8008/decode', decodedMessage);
          dispatch(fetchDecodeSuccess(response.data));
      } catch (error) {
          dispatch(fetchDecodeFailure(error));
      }
    };

};