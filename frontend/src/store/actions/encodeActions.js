import axios from "axios";

export const FETCH_ENCODE_REQUEST = 'FETCH_ENCODE_REQUEST';
export const FETCH_ENCODE_SUCCESS = 'FETCH_ENCODE_SUCCESS';
export const FETCH_ENCODE_FAILURE = 'FETCH_ENCODE_FAILURE';
export const ON_ENCODE_MESSAGE_CHANGE = 'ON_ENCODE_MESSAGE_CHANGE';
export const ON_PASSWORD_CHANGE = 'ON_PASSWORD_CHANGE';

export const fetchEncodeRequest = () => ({type: FETCH_ENCODE_REQUEST});
export const fetchEncodeSuccess = encodedMessage => ({type: FETCH_ENCODE_SUCCESS, payload: encodedMessage});
export const fetchEncodeFailure = error => ({type: FETCH_ENCODE_FAILURE, payload: error});
export const onPasswordChange = password => ({type: ON_PASSWORD_CHANGE, payload: password});
export const onEncodeMessageChange = message => ({type: ON_ENCODE_MESSAGE_CHANGE, payload: message});

export const fetchRequest = encodeMessage => {
    return async dispatch => {
        try {
            dispatch(fetchEncodeRequest());

            const response  = await axios.post('http://localhost:8008/encode', encodeMessage);
            dispatch(fetchEncodeSuccess(response.data));
        } catch (error) {
            dispatch(fetchEncodeFailure(error));
        }
    };

};
