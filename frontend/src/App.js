import './App.css';
import {fetchRequest, onEncodeMessageChange, onPasswordChange} from "./store/actions/encodeActions";
import {useDispatch, useSelector} from "react-redux";
import {fetchDecode, onDecodeMessageChange} from "./store/actions/decodeActions";

const App = () => {
    const dispatch = useDispatch();

    const decodeMessage = useSelector(state => state.decode.message);
    const password = useSelector(state => state.encode.password);
    const encodedMessage = useSelector(state => state.encode.encodedMessage);

    const onSubmit = e => {
        e.preventDefault();
    };

    const decodeMessageChange = message => {
        dispatch(onDecodeMessageChange(message));
    };

    const onPasswordChangeInput = password => {
        dispatch(onPasswordChange(password));
    };

    const onEncodedMessageChange = message => {
        dispatch(onEncodeMessageChange(message));
    }

    return (
        <form onSubmit={onSubmit}>
            <textarea value={decodeMessage} name="message" onChange={e => decodeMessageChange(e.target.value)}/>
            <input type="text" name="password" value={password} onChange={e => onPasswordChangeInput(e.target.value)}/>
            <div className="ButtonsContainer">
                <button
                    className="Encode"
                    onClick={() => dispatch(fetchRequest({message: decodeMessage, password}))}>Encode
                </button>
                <button
                    className="Decode"
                    onClick={() => dispatch(fetchDecode({password, message: encodedMessage}))}>Decode</button>
            </div>
            <textarea value={encodedMessage} onChange={e => onEncodedMessageChange(e.target.value)} name="decodeMessage"/>
        </form>
    )
};

export default App;
