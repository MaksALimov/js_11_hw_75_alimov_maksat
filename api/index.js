const express = require('express');
const {json} = require('express');
const cors = require('cors');
const cipher = require('./app/ciphers');

const app = express();
app.use(json());
app.use(cors());
const port = 8008;

app.use('/', cipher);

app.listen(port, () => {
    console.log('We are live on: ', 'http://127.0.0.1:8008');
});