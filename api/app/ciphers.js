const express = require('express');
const {Vigenere} = require("caesar-salad");
const router = express.Router();

const password = 'password';

router.get('/', (req, res) => {
    res.send(req.params.name);
});

router.get('/:name', (req, res) => {
    res.send(req.params.name);
});

router.get(`/encode`, (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(req.body.message));
});

router.get(`/decode`, (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(req.body.message));
});

router.post('/encode', (req, res) => {
    res.send({encoded: Vigenere.Cipher(req.body.password).crypt(req.body.message)});
});

router.post('/decode', (req, res) => {
    res.send({decoded: Vigenere.Decipher(req.body.password).crypt(req.body.message)});
});


module.exports = router;